package com.gogrillion.grafs.actions;

import com.gogrillion.grafs.filesystem.CrawlAction;
import com.gogrillion.grafs.filesystem.Tools;
import com.gogrillion.libgrafs.GraFs;
import com.gogrillion.libgrafs.GraFsException;
import com.gogrillion.libgrafs.filesystem.File;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Mike Grill on 1/12/15.
 *
 * Filesystem action consumer thread
 */

public class Consumer implements Runnable {

    private boolean producerDone = false;

    private GraFs db;
    private BlockingQueue<CrawlAction> queue;

    public Consumer(GraFs db, BlockingQueue<CrawlAction> actionQueue ){
        this.db = db;
        this.queue = actionQueue;
    }

    public void producerDone(){
        this.producerDone = true;
    }

    @Override
    public void run() {

        try {

            CrawlAction action;
            while ( true ) {

                action = queue.take();

                if( action.getActionType() == CrawlAction.CrawlActionType.Add ){
                    add(action.getPath());
                } else if( action.getActionType() == CrawlAction.CrawlActionType.Update ){
                    update(action.getPath());
                } else if( action.getActionType() == CrawlAction.CrawlActionType.Delete ){
                    deleteFile(action.getPath());
                }

            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void add(Path absPath){

        //System.out.println( "Add file: " + absPath );

        try {

            if (db.hasPath(absPath.toString())) {
                //Not an add, this should maybe be an update?
                //TODO Something
                System.err.println("Cannot add node, already exists: " + absPath);
                return;
            }

            //Resolve to absolute path
            absPath = absPath.toAbsolutePath();

            if (Files.isDirectory(absPath)) {
                db.addFolder(
                    absPath.toString(),
                    Files.getLastModifiedTime(absPath).toMillis() / 1000
                );

            } else if (Files.isRegularFile(absPath)) {
                db.addFile(
                    absPath.toString(),
                    Files.size(absPath),
                    Files.getLastModifiedTime(absPath).toMillis() / 1000,
                    Tools.getMimeType(absPath)
                );
            }

        } catch (GraFsException | IOException e ) {
            e.printStackTrace();
        }

    }

    private void update(Path absPath){

        //System.out.println( "Update file: " + absPath );

        try {

            if( Files.isRegularFile(absPath) ) {

                File currentFileIndex = db.getFile(absPath.toString());

                //If size is different OR last modified is different, update
                if( currentFileIndex.getSize() !=  Files.size(absPath) ||
                    currentFileIndex.getLastModified() != Files.getLastModifiedTime(absPath).toMillis() / 1000 ){

                    //Build a new file object
                    currentFileIndex = new File( absPath.toString(), Files.size(absPath),
                            Files.getLastModifiedTime(absPath).toMillis() / 1000 );

                    //Update with the new details
                    db.update(currentFileIndex);
                }

            }

        } catch (GraFsException | IOException e) {
            e.printStackTrace();
        }

    }

    private void deleteFile(Path absPath){
        System.out.println( "Delete file: " + absPath );
    }

}
