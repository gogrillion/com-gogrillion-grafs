package com.gogrillion.grafs;

import org.apache.commons.cli.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private static void printHelp(Options options){
        HelpFormatter hFormat = new HelpFormatter();
        hFormat.printHelp("GraFS -d path -a action [[-s type] -d path] [-p path] [-v]", options );
    }
    
    public static void main(String[] args) {

        Option dbPath = new Option("d", "Database Path.");
        Option action = new Option("a", "Action to perform. ( list | add | remove | reindex | backup | restore )");
        Option refPath = new Option("p", "Root path to add or remove or reindex. Use -a list to see a list of current root paths");
        Option help =   new Option("h", "Prints this message");
        Option verbose = new Option("v", "Verbose output");

        //Action is required and needs one argument
        action.setRequired(true);
        action.setArgs(1);

        //A reference path needs one argument ( if specified )
        refPath.setArgs(1);

        //Database path is only required for SqlLite or Titan
        dbPath.setRequired(true);
        dbPath.setArgs(1);

        Options options = new Options();
        options.addOption(help);
        options.addOption(action);
        options.addOption(refPath);
        options.addOption(dbPath);
        options.addOption(verbose);

        CommandLineParser parser = new GnuParser();
        try {

            CommandLine params = parser.parse(options, args);
            
            //Print help?
            if( params.hasOption( "h" ) ){
                printHelp(options);
                return;
            }

            //Initialize application
            String storagePath = params.getOptionValue('d');
            App app = new App(storagePath);
            app.open();

            System.out.println( "Opening grafs DB: " + storagePath );

            //Do action
            String userAction = params.getOptionValue('a');
            switch (userAction) {
                case "list":

                    //Get the path list
                    ArrayList<String> rootPaths = app.getRoots();

                    //Display the path list
                    System.out.println("Current root paths in database:");
                    if (rootPaths.size() > 0) {
                        for (String path : rootPaths) {
                            System.out.println("    " + path);
                        }
                    } else {
                        System.out.println("    " + "No root Paths.");
                    }

                    break;

                case "add":

                    String addPath = params.getOptionValue('p');
                    if (params.hasOption('v')) {
                        System.out.println("Adding root path: " + addPath);
                    }

                    //Save the path in the list of paths
                    if (app.addRoot(addPath)) {
                        System.out.println("New root path added. Indexing.");

                        //Index the path
                        if (params.hasOption('v')) {
                            System.out.println("Indexing root path: " + addPath);
                        }

                        //After we add the root, run an index on it
                        app.indexPath(Paths.get(addPath));

                    } else {
                        System.out.println("Path already exists. To update the index use the reindex action.");
                    }

                    break;
                case "remove":

                    //Remove the path indexes

                    //Remove path from path list
                    app.removeRoot(params.getOptionValue('p'));

                    break;
                case "reindex":

                    String reindexPath = params.getOptionValue('p');
                    System.out.printf("Reindexing path: %s%n", reindexPath);
                    app.indexPath(Paths.get(reindexPath));

                    break;
                case "backup":

                    System.out.println("Exporting");
                    app.backup("");
                    System.out.println("Export Complete");

                    break;
                case "restore":

                    System.out.println("Restore not implemented.");
                    app.restore("");
                    break;
            }

            //Safely close DB
            app.close();

        } catch (MissingOptionException missingArgs ){
            System.out.println(missingArgs.getMessage());
            printHelp(options);
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }

    }

}
