package com.gogrillion.grafs.filesystem;

import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicMatch;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by grillion on 12/6/14.
 * Simple static set of file-hash functions
 */
public class Tools {

    public static String getMimeType(Path file){
        // getMagicMatch accepts Files or byte[],
        // which is nice if you want to test streams
        try {
            MagicMatch match = Magic.getMagicMatch(file.toFile(), true);
            System.out.println(match.toString());
            return match.getMimeType();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String generateMD5(File file){
        return hashFile(file, "MD5");
    }

    public static String generateSHA1(File file){
        return hashFile(file, "SHA-1");
    }

    public static String generateSHA256(File file){
        return hashFile(file, "SHA-256");
    }

    public static String bytesPerSecond( float bytes, float seconds ){
        float bps = bytes / seconds;
        return FileUtils.byteCountToDisplaySize((long)bps ) + "/sec";
    }

    private static String convertByteArrayToHexString(byte[] arrayBytes) {
        StringBuilder stringBuilder = new StringBuilder();
        for (byte arrayByte : arrayBytes) {
            stringBuilder.append(Integer.toString((arrayByte & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return stringBuilder.toString();
    }

    private static String hashFile(File file, String algorithm) {

        System.out.println("Hashing " +  FileUtils.byteCountToDisplaySize(file.length()) + " file - " + file.toString() );
        long hashStart = DateTime.now().getMillis();


        try {
            FileInputStream inputStream = new FileInputStream(file);
            MessageDigest digest = MessageDigest.getInstance(algorithm);

            byte[] bytesBuffer = new byte[4096];
            int bytesRead = -1;

            while ((bytesRead = inputStream.read(bytesBuffer)) != -1) {
                digest.update(bytesBuffer, 0, bytesRead);
            }

            byte[] hashedBytes = digest.digest();

            long hashStop = DateTime.now().getMillis();
            float seconds = ((float)(hashStop - hashStart)) / 1000;
            System.out.println(String.format(
                "   Hashed %s in %.2f seconds at %s",
                file.getName(),
                seconds,
                bytesPerSecond( file.length(), seconds )
            ));

            inputStream.close();
            return convertByteArrayToHexString(hashedBytes);
        } catch (NoSuchAlgorithmException ignored){

        } catch( IOException ignored ) {

        }

        return "";
    }



}
