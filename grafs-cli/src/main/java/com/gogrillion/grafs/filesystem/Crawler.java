package com.gogrillion.grafs.filesystem;

import com.gogrillion.libgrafs.GraFs;
import com.gogrillion.libgrafs.GraFsException;
import com.gogrillion.libgrafs.filesystem.Folder;
import com.gogrillion.libgrafs.filesystem.File;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Mike Grill on 1/2/15.
 *
 * File system crawling thread
 */
public class Crawler implements Runnable {

    private Path rootPath;
    private GraFs grafs;
    private BlockingQueue<CrawlAction> q;

    public Crawler(Path rootPath, GraFs grafs, BlockingQueue<CrawlAction> q ){
        this.rootPath = rootPath.toAbsolutePath();
        this.grafs = grafs;
        this.q = q;
    }

    @Override
    public void run() {
        crawl(rootPath);
    }

    public void crawl( Path path ){

        //open path
        try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(path)) {

            //Read indexed list
            Folder indexedFolder = grafs.getFolder(path.toString());

            ArrayList<File> indexFiles = grafs.getFiles(indexedFolder);

            //Check the size and then the date vs the index for each file.
            // For each found file, remove it from the list of known files.
            // After all files are checked, remove remaining files & folders
            // which were not found from the index.

            for (Path p : dirStream) {

                try {

                    String absPath = p.toString();

//                    if (grafs.hasPath(absPath)) {
//                        q.put(new CrawlAction(CrawlAction.CrawlActionType.Update, f.toString()));
//                    } else {
//                        q.put(new CrawlAction(CrawlAction.CrawlActionType.Add, f.toString()));
//                    }
//
//                    if (Files.isRegularFile(p)) {
//
//                    } else if( Files.isSymbolicLink(p) ){
//
//                    } else if (Files.isDirectory(p)) {
//                        crawl(p);//recurse
//                    }

                } catch (Exception e) {
                    //System.out.println("        Cannot read file: " + e.getMessage());
                    e.printStackTrace();
                }

            }

        } catch (IOException | GraFsException e ){
            e.printStackTrace();
        }
    }

}
