package com.gogrillion.grafs.filesystem;

import com.gogrillion.grafs.actions.GrafsAction;

import java.nio.file.Path;

/**
 * Created by Mike Grill on 1/2/15.
 *
 * Wrapper for a crawl action and associated data
 */
public class CrawlAction implements GrafsAction {

    public enum CrawlActionType {
        Add,
        Update,
        Delete
    };

    protected CrawlActionType action;
    private Path path;

    public CrawlAction( CrawlActionType action, Path path){
        this.action = action;
        this.path = path;
    }

    public CrawlActionType getActionType() {
        return action;
    }

    public void setAction(CrawlActionType action) {
        this.action = action;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }





}
