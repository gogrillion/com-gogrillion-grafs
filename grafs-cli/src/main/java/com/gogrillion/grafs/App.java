package com.gogrillion.grafs;

import com.gogrillion.grafs.actions.Consumer;
import com.gogrillion.grafs.filesystem.CrawlAction;
import com.gogrillion.grafs.filesystem.Crawler;
import com.gogrillion.libgrafs.GraFs;
import com.gogrillion.libgrafs.GraFsException;
import com.gogrillion.libgrafs.filesystem.Folder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Mike Grill on 4/20/15.
 */
public class App {

    //Filesystem location of database
    private String dbPath;

    //Libgrafs instance
    private GraFs g;

    //Initialize a new cli tool 'app' instance
    public App(String dbPath){
        this.dbPath = dbPath;
    }

    /**
     * Internal management of libgrafs
     * @return true on success, false on failure
     */
    public boolean open(){
        try{
            g = new GraFs(this.dbPath);
            g.open();
        }catch (Exception e){
            System.err.println(e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Close libgrafs db
     */
    public void close(){
        g.close();
    }

    /**
     * Get root paths
     * @return A list of root paths
     */
    public ArrayList<String> getRoots(){
        ArrayList<Folder> roots;
        ArrayList<String> stringRoots = new ArrayList<>();
        try {
            roots = g.getRoots();
            for( Folder root: roots ){
                stringRoots.add(root.getAbsPath());
            }
        } catch (GraFsException e) {
            e.printStackTrace();
        }
        return stringRoots;
    }

    /**
     * Export a filesystems index
     */
    public void backup( String filename ){
        try{
            g.backup(filename);
        } catch (GraFsException e) {
            e.printStackTrace();
        }
    }

    /**
     * Restore a filesystem index from backup
     */
    public void restore( String filename ){
        try{
            g.restore(filename);
        } catch (GraFsException e) {
            e.printStackTrace();
        }
    }

    /**
     * Add a root path and index it's children
     * @param absRootPath absolute path of root folder to add
     * @return true on success
     */
    public boolean addRoot( String absRootPath ){
        Path newRoot = Paths.get(absRootPath);
        try{
            //Add the root path to index
            g.addRoot(
                newRoot.toAbsolutePath().toString(),
                Files.getLastModifiedTime(newRoot).toMillis() / 1000
            );

            //Index Path recursively
            indexPath(newRoot);

        } catch (GraFsException | IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Remove a root path and it's indexed children
     * @param absRootPath absolute path of root folder to remove
     * @return true on success
     */
    public boolean removeRoot( String absRootPath ){
        try {
            g.remove(absRootPath);
        } catch (GraFsException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Re-index a local filesystem path.
     * @param path index a folder and it's children recursively
     * @throws IOException
     */
    public void indexPath(Path path) throws IOException {

        //One thread to iterate over the filesystem
        //  This thread will add files and folders to queues for
        //  New, Existing, Old
        BlockingQueue<CrawlAction> actionQueue = new ArrayBlockingQueue<>(100);

        //Crawl thread
        Crawler spider = new Crawler(path, g, actionQueue);
        Thread crawlThread = new Thread(spider);
        crawlThread.start();

        ExecutorService consumerPool = Executors.newFixedThreadPool(10);
        for (int i = 0; i < 10; i++) {
            consumerPool.submit(new Consumer(g, actionQueue));
        }
        //Seperate thread to consume actions
//        Consumer actionConsumer = new Consumer( db, actionQueue );
//        Thread consumerThread = new Thread( actionConsumer );
//        consumerThread.start();

        //One thread pool to process items from queue.
        try {
            crawlThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //consumerPool.shutdown();
        //Finish consuming
//        try {
//            consumerThread.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

    }

    /*
    public void addFile(){//Generate a hash and add to the index.
        String md5 = Tools.generateMD5(f);

        //Add to index
        db.add(f, md5);
    }

    public void updateFile(){
        //Get current indexed file
        IndexFile fi = db.getIndexFile(absPath);

        //Remove from existing known files in this path
        //indexFiles.

        //Is file size different? and if not, is the time stamp different?
        //If so get a new md5
        if (fi.getSize() != f.length() ||
                fi.getLastModified() != f.lastModified()) {

            //Generate a new hash to compare to the index
            String md5 = Tools.generateMD5(f);

            if (fi.getMd5() != md5) {
                System.out.println("    File has changed: " + absPath);
                db.update(f, md5);
            }
        } else {
            System.out.println("        File is up to date.");
        }
    }
*/

}