package com.gogrillion.libgrafs.storage;

import com.gogrillion.libgrafs.filesystem.BaseObject;
import com.gogrillion.libgrafs.filesystem.File;
import com.gogrillion.libgrafs.filesystem.Folder;

import com.thinkaurelius.titan.core.*;
import com.thinkaurelius.titan.core.schema.TitanManagement;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.nio.file.Path;
import java.util.*;

/**
 * Created by Mike Grill on 11/23/14.
 *
 * Manages a TitanGraph Storage engine
 *
 * VertexLabels
 *  config
 *  folder
 *    isRoot
 *    name
 *    absPath
 *    lastModified
 *  file (extends folder)
 *    size
 *  fileHash
 *    md5
 *
 * EdgeLabels
 *  root
 *  folder
 *  file
 *  hash
 *
 */
public class Titan extends GrafsDb<Vertex> {

    protected Path dbPath;
    protected TitanGraph titanGraph;

    protected Vertex configVertex;

    public Titan(Path dbPath) {
        //Ensure dbPath is absolute
        this.dbPath = dbPath.toAbsolutePath();
    }

    /* ***********************************************************************************
     * Private, titan-specific support methods
     * ***********************************************************************************/

    /**
     * Check to see if a vertex label exists, if not then create it
     * @param ms Titan Management interface instance
     * @param label Vertex label to allow
     */
    private void ensureVertexLabel( TitanManagement ms, String label){
        if( !ms.containsVertexLabel(label) ) {
            ms.makeVertexLabel(label).make();
        }
    }

    /**
     * Check to see if an edge label exists, if not then create it
     * @param ms Titan Management interface instance
     * @param label Edge label to allow
     */
    private void ensureEdgeLabel(TitanManagement ms, String label){
        if( !ms.containsEdgeLabel(label)){
            ms.makeEdgeLabel(label).make();
        }
    }

    /**
     * * Check to see if a property key exists, if not then create it
     * @param ms Titan Management interface instance
     * @param propertyKey property key to manage
     * @param c Data type Class for property value
     */
    private void ensureVertexPropertyKeyIndex(TitanManagement ms, String propertyKey, Class<?> c){
        if ( !ms.containsPropertyKey(propertyKey)) {
            PropertyKey vPropAbsPath = ms.makePropertyKey(propertyKey).dataType(c).make();
            ms.buildIndex("by" + propertyKey, Vertex.class).addKey(vPropAbsPath).buildCompositeIndex();
        }
    }

    /**
     * Create a 'Folder' vertex
     * @param newFolder Folder to index
     * @return new Folder vertex
     */
    private Vertex createFolderVertex( Folder newFolder ){
        Vertex v = titanGraph.addVertexWithLabel("folder");

        //Add folder properties
        v.setProperty("absPath", newFolder.getAbsPath());
        v.setProperty("name", newFolder.getFilename());
        v.setProperty("lastModified", newFolder.getLastModified());

        //Only add isRoot to roots
        if( newFolder.isRoot() ){
            v.setProperty( "isRoot", true );
        }

        //Save and return the new vertex
        titanGraph.commit();
        return v;
    }

    /**
     * Create a Vertex of type 'file'
     * @param newFile File object to index
     * @return new Vertex of File
     */
    private Vertex createFileVertex( File newFile ){

        Vertex v = titanGraph.addVertexWithLabel("file");
        //Add folder properties
        v.setProperty("absPath", newFile.getAbsPath());
        v.setProperty("name", newFile.getFilename());
        v.setProperty("lastModified", newFile.getLastModified());
        v.setProperty("size", newFile.getSize());
        v.setProperty("mimeType", newFile.getMimeType());


        titanGraph.commit();
        return v;
    }

    /**
     * Create a vertex to represent a MD5 hash
     * @param md5 MD5 Hash of a file
     * @return Vertex of new hash
     */
    private Vertex createMd5Vertex( String md5 ){

        Vertex vHash = titanGraph.addVertexWithLabel("md5");

        //Add properties
        vHash.setProperty("md5", md5 );

        titanGraph.commit();
        return vHash;
    }

    /**
     * Create an edge from parent to a child of type 'type'
     * @param parent Titan vertex of parent folder
     * @param child Titan vertex of child folder
     * @param type Edge type
     * @return New directed edge  of type from parent to child
     */
    private Edge createEdge( Vertex parent, Vertex child, String type ){
        //In edge from parent folder
        Edge newE = titanGraph.addEdge(null, parent, child, type);
        titanGraph.commit();
        return newE;
    }



    /* ***********************************************************************************
     * GrafsDb methods
     * ***********************************************************************************/


    @Override
    public boolean open() {

        Configuration titanOptions = new PropertiesConfiguration();
        titanOptions.setProperty("schema.default", "none");
        titanOptions.setProperty("storage.backend", "berkeleyje");
        titanOptions.setProperty("storage.directory", this.dbPath.toString() );

        this.titanGraph = TitanFactory.open(titanOptions);

        //Schema
        this.ensureSchema();

        return true;
    }

    @Override
    public boolean close() {
        if (titanGraph != null){
            titanGraph.shutdown();
        }
        return true;
    }

    @Override
    public void rollback(){
        titanGraph.rollback();
    }


    /**
     * GrafsDb template function to convert storage objects to a Folder model
     * @param record Titan Vertex
     * @return Folder object
     */
    @Override
    protected Folder toFolder(Vertex record) {
        return new Folder(
            (String)record.getProperty("absPath"),
            (long)record.getProperty("lastModified"),
            (boolean)record.getProperty("isRoot")
        );
    }

    /**
     * GrafsDb template function to convert storage objects to a File model
     * @param record Titan Vertex
     * @return File object
     */
    @Override
    protected File toFile(Vertex record) {
        return new File(
            (String)record.getProperty("absPath"),
            (long)record.getProperty("size"),
            (long)record.getProperty("lastModified"),
            (String)record.getProperty("mimeType")
        );
    }

    /**
     * Configure this Titan graph instance to support the schema we need.
     * Configuring this up-front can greatly increase performance in large data sets
     */
    @Override
    protected void ensureSchema() {

        TitanManagement ms = this.titanGraph.getManagementSystem();

        //Vertex labels to manage
        String[] vertexLabels = new String[]{
            //room for configuration settings
            "@config",

            //Filesystem Object types
            "device", "fifo", "file", "folder", "socket", "symlink", "unknown",

            //Indexed metadata used for reverse references
            "hash"
        };

        //Edge labels to manage
        String[] edgeLabels = new String[]{
            //Filesystem Object types
            "root", "folder", "file",

            //Indexed metadata used for reverse references
            "md5"
        };

        //Ensure all vertex labels
        for( String vLabel: vertexLabels ){
            ensureVertexLabel(ms, vLabel);
        }

        //Ensure all Edge Labels
        for( String eLabel: edgeLabels ){
            this.ensureEdgeLabel(ms, eLabel);
        }

        //Ensure vertex property indexes
        this.ensureVertexPropertyKeyIndex(ms, "absPath", String.class);
        this.ensureVertexPropertyKeyIndex(ms, "isRoot", Boolean.class);
        this.ensureVertexPropertyKeyIndex(ms, "name", String.class);
        this.ensureVertexPropertyKeyIndex(ms, "lastModified", Long.class);
        this.ensureVertexPropertyKeyIndex(ms, "size", Long.class);
        //this.ensureVertexPropertyKeyIndex(ms, "md5", String.class);

        //Save possible graph management changes
        ms.commit();

        //Create root vertex ( config vertex ) if it doesn't exist
        configVertex = getByAbsPath("@config");
        if( configVertex == null ) {
            configVertex = titanGraph.addVertexWithLabel("@config");
        }

        this.titanGraph.commit();
    }

    /**
     * Print all objects, links, and properties.
     */
    @Override
    public void backup( String filename ) {
        Iterable<Vertex> allVertices = titanGraph.getVertices();
        for (Vertex v : allVertices) {
            System.out.println("v[" + v.getProperty("label") + ":" + v.getId()+"]");
            for(String p: v.getPropertyKeys()){
                String val = v.getProperty(p).toString();
                System.out.println("  " + p + " = " + val);
            }
        }

        Iterable<Edge> allEdges = titanGraph.getEdges();
        for (Edge e : allEdges) {
            System.out.println("e[" + e.getLabel() + ":" + e.getId() + "]");
            for(String p: e.getPropertyKeys()){
                String val = e.getProperty(p).toString();
                System.out.println("  " + p + " = " + val);
            }
        }
    }

    @Override
    public void restore(String filename) {

    }

    @Override
    public Folder addFolder(String absPath, long lastModified, boolean isRoot) throws StorageException {

        if( this.hasPath(absPath) ){
            throw new StorageException("Cannot add folder. Folder is already in database.");
        }

        //If we are adding a "root", then we don't resolve the parent
        // we will create a 'root' edge from @config to the root.
        Vertex v;
        Folder newFolder = new Folder( absPath, lastModified, isRoot );
        if( isRoot ){

            //Create vertex
            v = this.createFolderVertex( newFolder );
            createEdge( configVertex, v, "root" );

        } else {

            //We're adding a normal path, not a root so we need to figure out what
            // the parent path would be ( error if none ) and ensure that vertex exists
            // so we can create a 'child' edge from the parent vertex.
            String parentPath = newFolder.getParentPath();

            //If parent is missing, there's an error.
            Vertex vParent = this.getByAbsPath(parentPath);
            if( vParent == null ){
                throw new StorageException("Cannot add folder. Parent folder has not been added.");
            }

            //We have the parent, lets make our new vertex
            v = this.createFolderVertex( newFolder );
            createEdge( vParent, v, "folder" );

        }

        //Persist
        titanGraph.commit();

        //return folder object
        return newFolder;
    }

    @Override
    public File addFile(String absPath, long size, long lastModified, String mimeType) throws StorageException {

        //Does Vertex exist?
        if( this.hasPath(absPath) ){
            throw new StorageException("Cannot add file, vertex already exists: " + absPath );
        }

        //If parent is missing, there's an error.
        File newFile = new File( absPath, size, lastModified, mimeType );
        String parentPath = newFile.getParentPath();
        Vertex vParent = this.getByAbsPath(parentPath);

        //Ensure the parent is found successfully
        if( vParent == null ){
            throw new StorageException("Cannot add file, parent vertex is missing: " + parentPath );
        }

        //Create file vertex
        Vertex vFile = this.createFileVertex( newFile );

        //Folder->file edge
        this.createEdge( vParent, vFile, "file");

        //If an MD5 was given,...
        // TODO ADD MD5
        //if( null != md5 ){

            //Get existing hash or make a new one
            //Vertex vHash = this.getHashVertexByMD5(md5);
            //if( vHash == null ){
            //    vHash = createHashVertexMD5(md5);
            //}

            //Link to the hash
            //Edge hashEdge = this.createHashEdge(vFile, vHash);
        //}

        //Persist
        titanGraph.commit();

        //return the new file object
        return newFile;
    }


    @Override
    /**
     * Check for existence of an indexed node by absolute path
     */
    public boolean hasPath(String absPath) {
        return titanGraph.getVertices("absPath", absPath).iterator().hasNext();
    }

    /**
     * Get a vertex by absolute path
     * @param absPath absolute path of file
     * @return Vertex of
     */
    protected Vertex getByAbsPath( String absPath ){
        Iterator<Vertex> itr = titanGraph.getVertices("absPath", absPath).iterator();
        if( itr.hasNext() ) {
            return itr.next();
        }
        return null;
    }

    /**
     * Get a hash object by it's md5 value
     * @param md5 The MD5 value as a string
     * @return Hash object Vertex
     */
    private Vertex getMd5(String md5) {
        Iterator<Vertex> itr = titanGraph.getVertices("md5", md5).iterator();
        if( itr.hasNext() ) {
            return itr.next();
        }
        return null;
    }

    /**
     * Get a list of root folders
     * @return ArrayList of root folders
     */
    @Override
    public ArrayList<Folder> getRoots() {
        ArrayList<Folder> roots = new ArrayList<>();
        Iterable<Vertex> vRoots = configVertex.getVertices(Direction.OUT, "folder");
        for (Vertex vRoot : vRoots) {

            //Create a new Folder instance and add it to the list
            roots.add( toFolder(vRoot) );
        }

        //Return the list of Folders
        return roots;
    }

    /**
     * Get a Folder by absolute path
     * @param absFilePath Absolute path of folder
     * @return Folder object
     */
    @Override
    public Folder getFolder(String absFilePath) throws StorageException {
        Vertex folderVertex = getByAbsPath(absFilePath);
        if( null != folderVertex ){
            toFolder( folderVertex );
        }
        return null;
    }

    /**
     * Get a list of sub folder within a given folder
     * @param parent Parent folder
     * @return An array of Folder objects
     * @throws StorageException
     */
    @Override
    public ArrayList<Folder> getFolders(Folder parent) throws StorageException {

        //Result storage
        ArrayList<Folder> folders = new ArrayList<>();

        //Parent folder vertex
        Vertex vParent = getByAbsPath(parent.getAbsPath());

        //Iterate over folder edges
        Iterable<Vertex> vChildren = vParent.getVertices(Direction.OUT, "folder");
        for( Vertex vSub: vChildren){
            folders.add( toFolder(vSub) );
        }

        return folders;
    }

    /**
     * Get a File by absolute path
     * @param absFilePath Absolute path of file
     * @return File object
     */
    @Override
    public File getFile(String absFilePath) throws StorageException {
        Vertex fileVertex = getByAbsPath(absFilePath);
        if( null != fileVertex ){
            return toFile( fileVertex );
        }
        return null;
    }

    /**
     * Get a list of files within a given folder
     * @param parent Parent folder
     * @return An array of File objects
     * @throws StorageException
     */
    @Override
    public ArrayList<File> getFiles(Folder parent) throws StorageException {
        //Result storage
        ArrayList<File> files = new ArrayList<>();

        //Parent folder vertex
        Vertex vParent = getByAbsPath(parent.getAbsPath());

        //Iterate over folder edges
        Iterable<Vertex> vChildren = vParent.getVertices(Direction.OUT, "file");
        for( Vertex vSub: vChildren){
            files.add( toFile(vSub) );
        }

        return files;
    }

    @Override
    public boolean update(Folder folder)throws StorageException {

        //Get current index
        Vertex vFolder = getByAbsPath(folder.getAbsPath());

        //Ensure the vertex currently exists
        if( null == vFolder ){
            throw new StorageException("Cannot perform folder update, vertex is not found: " + folder.getAbsPath());
        }

        vFolder.setProperty("absPath", folder.getAbsPath());
        vFolder.setProperty("name", folder.getFilename());
        vFolder.setProperty("lastModified", folder.getLastModified());
        vFolder.setProperty("isRoot", folder.isRoot());

        //Persist
        titanGraph.commit();

        return true;
    }

    @Override
    public boolean update(File file) throws StorageException {

        //Get current index
        Vertex vFile = getByAbsPath(file.getAbsPath());

        //Ensure the vertex currently exists
        if( null == vFile ){
            throw new StorageException("Cannot perform file update, vertex is not found: " + file.getAbsPath());
        }

        //Update file properties
        vFile.setProperty("absPath", file.getAbsPath());
        vFile.setProperty("name", file.getFilename());
        vFile.setProperty("size", file.getSize());
        vFile.setProperty("lastModified", file.getLastModified());
        vFile.setProperty("mimeType", file.getMimeType());

        //TODO ADD MD5
        //See if there's a 'hash' edge ( there should be unless cancelled during index )
        // and check if the hash matches. If not, delete the edge and make an edge to
        // a new or existing hash vertex by md5
//        Iterator<Edge> hashItr = vFile.getEdges(Direction.OUT, "hash" ).iterator();
//
//        //If there an edge
//        if( hashItr.hasNext() ) {
//
//            //Get outE->outV
//            Edge eHash = hashItr.next();
//            Vertex vHash = eHash.getVertex(Direction.OUT);
//
//            //If it's not the right hash...
//            if( !md5.equals( vHash.getProperty("md5") ) ){
//
//                //Remove the old link
//                eHash.remove();
//
//                //
//                Vertex newHashV = this.getHashVertexByMD5(md5);
//                if( newHashV == null ){
//                    newHashV = titanGraph.addVertexWithLabel("fileHash");
//                }
//                //Make the new has edge
//                Edge newEdge = titanGraph.addEdge(null, v, newHashV, "hash");
//            }
//        }

        //Persist
        titanGraph.commit();

        return true;
    }


    /**
     * Remove a vertex and it's isolated children from the graph
     * @param path Absolute path to remove.
     * @return Number of objects removed.
     */
    @Override
    public int remove(String path) {
        return 0;
    }


    public ArrayList<BaseObject> getIndexChildren(String absFolderPath) {
        ArrayList<BaseObject> files = new ArrayList<>(0);
        Vertex parent = getByAbsPath(absFolderPath);
        if( parent == null ){
            return files;
        }
        Iterable<Vertex> vChildren = parent.getVertices(Direction.OUT, "file", "folder" );
        for (Vertex v : vChildren) {
            if( v.getProperty("label") == "file" ) {
                files.add(new com.gogrillion.libgrafs.filesystem.File(
                        (String) v.getProperty("absPath"),
                        (long) v.getProperty("lastModified"),
                        (long) v.getProperty("size"),
                        (String) v.getProperty("md5")
                ));
            }else if( v.getProperty("label") == "folder" ) {
                files.add( toFolder(v) );
            }
        }
        return files;
    }

}
