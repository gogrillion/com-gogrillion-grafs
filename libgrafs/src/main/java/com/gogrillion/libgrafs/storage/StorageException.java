package com.gogrillion.libgrafs.storage;

/**
 * Created by Mike Grill on 5/5/15.
 * Exception for storage engines
 */
public class StorageException extends Exception {

    public StorageException(String message){
        super(message);
    }
}
