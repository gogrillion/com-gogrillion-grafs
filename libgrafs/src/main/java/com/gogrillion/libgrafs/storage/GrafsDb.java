package com.gogrillion.libgrafs.storage;

import com.gogrillion.libgrafs.filesystem.File;
import com.gogrillion.libgrafs.filesystem.Folder;

/**
 * Created by Mike Grill on 4/20/15.
 *
 * Base implementation of the iGraFS interface.
 * Titan extends this and uses Vertex to represent DB objects.
 * SQL Would use SqlRow
 * Mongo would use Document
 */
public abstract class GrafsDb<T> implements IGrafsDb {

    protected abstract Folder toFolder(T record);
    protected abstract File toFile(T record);

    protected abstract void ensureSchema();
    protected abstract T getByAbsPath(String absPath);

}
