package com.gogrillion.libgrafs.storage;

import com.gogrillion.libgrafs.filesystem.BaseObject;
import com.gogrillion.libgrafs.filesystem.File;
import com.gogrillion.libgrafs.filesystem.Folder;

import java.util.ArrayList;

/**
 * Created by Mike Grill on 11/20/14.
 *
 * Defines all required functions for GraFS to manage path and file information
 * These need to be implemented by any storage engine built.
 */
public interface IGrafsDb {

    //Database
    public boolean  open() throws StorageException;
    public boolean  close();
    public void     rollback();

    //Import / Export
    public void   backup( String filename ) throws StorageException;
    public void  restore( String filename ) throws StorageException;

    //Create
    public Folder   addFolder( String absPath, long lastModified, boolean isRoot ) throws StorageException;
    public File     addFile ( String absPath, long size, long lastModified, String mimeType ) throws StorageException;

    // Getters
    public ArrayList<Folder>  getRoots() throws StorageException;
    public boolean  hasPath( String absPath ) throws StorageException;
    public File     getFile( String absFilePath) throws StorageException;
    public Folder   getFolder( String absFilePath ) throws StorageException;

    //Get Children
    public ArrayList<Folder>  getFolders( Folder parent) throws StorageException;
    public ArrayList<File>    getFiles( Folder parent ) throws StorageException;

    //Update Element
    public boolean  update( Folder folder ) throws StorageException;
    public boolean  update( File file ) throws StorageException;

    //Remove only needs abs path and works for all objects
    // Recursively traverse the graph and remove node with no InE.
    // Remove all incomplete edges
    public int      remove( String absPath ) throws StorageException;

}
