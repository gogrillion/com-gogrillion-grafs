package com.gogrillion.libgrafs;

/**
 * Custom exception for libgrafs
 */
public class GraFsException extends Exception {

    public GraFsException( String message ){
        super(message);
    }
    public GraFsException( Exception se ){
        super(se);
    }
}
