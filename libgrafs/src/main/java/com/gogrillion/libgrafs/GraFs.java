package com.gogrillion.libgrafs;

import com.gogrillion.libgrafs.filesystem.File;
import com.gogrillion.libgrafs.filesystem.Folder;
import com.gogrillion.libgrafs.storage.GrafsDb;
import com.gogrillion.libgrafs.storage.StorageException;
import com.gogrillion.libgrafs.storage.Titan;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.ArrayList;

/**
 * Created by Mike Grill on 4/20/15.
 *
 * Public interface to libgrafs.
 */
public class GraFs {

    //Database path
    private Path dbPath;

    //internal database instance
    GrafsDb g;

    /**
     * Get ready to open the database.
     */
    public GraFs(String graphDatabasePath) throws GraFsException {

        Path dbPath = Paths.get(graphDatabasePath);

        //Does path exist?
        if( !Files.exists(dbPath)){
            throw new GraFsException("Database path does not exist.");
        }

        if( !Files.isDirectory(dbPath)){
            throw new GraFsException("Database path is not a folder.");
        }

        //Path exists and is a folder.
        this.dbPath = dbPath;
    }

    /**
     * Initialize Titan backend
     * @throws GraFsException
     */
    public void open() throws GraFsException {

        try{
            //Titan connector accepts a Path
            g = new Titan(this.dbPath);
            g.open();

        } catch (Exception e){
            throw new GraFsException(e);
        }
    }

    /**
     * Close titan backend
     */
    public void close(){
        g.close();
    }

    /**
     * Test for existence of an index by absolute path
     * @return True is a vertex of any type is found.
     */
    public boolean hasPath( String absPath ) throws GraFsException {
        try {
            return g.hasPath(absPath);
        } catch (StorageException e) {
            throw new GraFsException(e);
        }
    }

    /**
     * Add a 'root' folder. This represent the top level directory that will be indexed.
     * @param absPath Absolute path of top level
     * @param lastModified UNIX Timestamp of last modification
     * @return Folder object
     * @throws GraFsException
     */
    public Folder addRoot( String absPath, long lastModified ) throws GraFsException {
        try{
            return g.addFolder( absPath, lastModified, true );
        } catch( StorageException e ){
            g.rollback();
            throw new GraFsException(e);
        }
    }

    /**
     * Add a folder to the graph
     * @param absPath Absolute path of folder
     * @param lastModified UNIX Timestamp of last modification
     * @return Folder object
     * @throws GraFsException
     */
    public Folder addFolder( String absPath, long lastModified ) throws GraFsException {
        try{
            return g.addFolder( absPath, lastModified, false );
        } catch( StorageException e ){
            g.rollback();
            throw new GraFsException(e);
        }
    }

    /**
     * Add a file to the index.
     * @param absPath Absolute path of file
     * @param size File size
     * @param lastModified UNIX Timestamp of last modification
     * @param mimeType mime type
     * @return File object
     * @throws GraFsException
     */
    public File addFile( String absPath, long size, long lastModified, String mimeType ) throws GraFsException {
        try{
            return g.addFile(absPath, size, lastModified, mimeType);
        } catch( StorageException e ){
            g.rollback();
            throw new GraFsException(e);
        }
    }


    /**
     * Get a list of root paths
     * @return An array of Folder objects defined as roots in the graph
     */
    public ArrayList<Folder> getRoots() throws GraFsException {
        try {
            return g.getRoots();
        } catch (StorageException e) {
            throw new GraFsException(e);
        }
    }

    /**
     * Get the indexed record for a folder using the absolute filesystem path
     * @param absPath Absolute filesystem path
     * @return libgrafs Folder object
     */
    public Folder getFolder( String absPath ) throws GraFsException {
        try {
            return g.getFolder(absPath);
        } catch (StorageException e) {
            throw new GraFsException(e);
        }
    }

    /**
     * Get the indexed sub folders within a folder
     * @param parent Parent folder index object
     * @return list of libgrafs folders
     */
    public ArrayList<Folder> getFolders( Folder parent ) throws GraFsException{
        try {
            return g.getFolders(parent);
        } catch (StorageException e) {
            throw new GraFsException(e);
        }
    }

    /**
     * Get the indexed files within a folder
     * @param parent Parent folder index object
     * @return list of libgrafs files
     */
    public ArrayList<File> getFiles( Folder parent ) throws GraFsException{
        try {
            return g.getFiles(parent);
        } catch (StorageException e) {
            throw new GraFsException(e);
        }
    }

    /**
     * Get the indexed information about a single file
     * @param absPath Absolute path of file
     * @return File object
     * @throws GraFsException
     */
    public File getFile( String absPath ) throws GraFsException {
        try {
            return g.getFile(absPath);
        } catch (StorageException e) {
            throw new GraFsException(e);
        }
    }

    /**
     * Update a Folder index
     * @param folder Folder object with folder details
     * @return true on success
     * @throws GraFsException
     */
    public boolean update( Folder folder) throws GraFsException{
        try{
            return g.update(folder);
        } catch (StorageException e) {
            throw new GraFsException(e);
        }
    }

    /**
     * Update a File index
     * @param file file object with file details
     * @return true on success
     * @throws GraFsException
     */
    public boolean update( File file) throws GraFsException{
        try{
            return g.update(file);
        } catch (StorageException e) {
            throw new GraFsException(e);
        }
    }

    /**
     * Remove a portion of the indexed tree.
     * @param absPath Absolute path of object to remove.
     * @return Number of objects removed.
     * @throws GraFsException
     */
    public int remove( String absPath ) throws GraFsException{
        return 0;
    }

    /**
     * Backup a graph to a single file.
     * @param filename File to create
     * @throws GraFsException
     */
    public void backup( String filename ) throws GraFsException{
        try{
            g.backup(filename);
        } catch (StorageException e) {
            throw new GraFsException(e);
        }
    }

    /**
     * Restore a graph from a single file.
     * @param filename Backup made from backup function
     * @throws GraFsException
     */
    public void restore( String filename ) throws GraFsException{
        try{
            g.restore(filename);
        } catch (StorageException e) {
            throw new GraFsException(e);
        }
    }

}