package com.gogrillion.libgrafs.filesystem;

/**
 * Created by Mike Grill on 4/20/15.
 * Object representing regular files
 */
public class File extends BaseObject {

    private String type = "file";
    private long size;
    private String mimeType;

    public File(String absPath, long lastModified, long size ) {
        this( absPath, lastModified, size, "unknown" );
    }

    public File(String absPath, long lastModified, long size, String mimeType ) {

        //Init node
        super(absPath, lastModified);

        //File props
        this.size = size;
        this.mimeType = mimeType;

    }

    @Override
    public String getType() {
        return this.type;
    }

    public long getSize(){
        return this.size;
    }

    public String getMimeType(){ return this.mimeType; }

}
