package com.gogrillion.libgrafs.filesystem;

/**
 * Created by Mike Grill on 4/20/15.
 *
 * Object implements all basic filesystem properties.
 */
public abstract class BaseObject {

    protected String type;
    protected String absPath;
    protected long lastModified;

    public BaseObject( String absPath, long lastModified ){
        this.absPath = absPath;
        this.lastModified = lastModified;
    }

    /**
     * Get the type of this object as a string.
     * @return Type of indexed object
     */
    public abstract String getType();

    /**
     * Get the absolutePath value
     * @return Absolute path and filename of object
     */
    public String getAbsPath(){ return absPath; }

    /**
     * Build a path that would represent the parent of a given path
     * @return parent absolute path
     */
    public String getParentPath(){
        return absPath.substring(0, absPath.lastIndexOf("/"));
    }

    /**
     * Get the filename portion of a absPath
     * @return parent absolute path
     */
    public String getFilename(){
        return absPath.substring( absPath.lastIndexOf("/") + 1);
    }

    /**
     * Get the last modified timestamp as a unix long
     * @return UNIX Timestamp as long
     */
    public long getLastModified(){
        return this.lastModified;
    }

    /**
     * Get the string value of this object, we use the absolute path
     * @return Absolute path and filename of object
     */
    public String toString(){
        return this.getAbsPath();
    }

}
