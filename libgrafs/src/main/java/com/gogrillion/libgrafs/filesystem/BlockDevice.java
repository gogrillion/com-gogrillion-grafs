package com.gogrillion.libgrafs.filesystem;

/**
 * Created by Mike Grill on 4/20/15.
 *
 * Object representing block devices
 */
public class BlockDevice extends Device {

    private String deviceType = "block";

    public BlockDevice(String absPath, long lastModified) {
        super(absPath, lastModified);
    }

    @Override
    public String getDeviceType() {
        return this.deviceType;
    }
}
