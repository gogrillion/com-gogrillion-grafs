package com.gogrillion.libgrafs.filesystem;

/**
 * Created by Mike Grill on 4/20/15.
 *
 * Object representing a unix socket
 */
public class Socket extends BaseObject {

    private String type = "socket";

    public Socket(String absPath, long lastModified) {
        super(absPath, lastModified);
    }

    @Override
    public String getType() {
        return this.type;
    }
}
