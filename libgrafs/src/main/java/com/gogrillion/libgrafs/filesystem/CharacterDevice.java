package com.gogrillion.libgrafs.filesystem;

/**
 * Created by Mike Grill on 4/20/15.
 *
 * Object representing character devices
 */
public class CharacterDevice extends Device {

    private String deviceType = "character";

    public CharacterDevice(String absPath, long lastModified) {
        super(absPath, lastModified);
    }

    @Override
    public String getDeviceType() {
        return this.deviceType;
    }
}
