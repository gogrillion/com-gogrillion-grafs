package com.gogrillion.libgrafs.filesystem;

/**
 * Created by Mike Grill on 4/20/15.
 * Indexed Symlink
 */
public class SymbolicLink extends BaseObject {

    private String type = "symlink";

    public SymbolicLink(String absPath, long lastModified) {
        super(absPath, lastModified);
    }

    @Override
    public String getType(){
        return this.type;
    }

}
