package com.gogrillion.libgrafs.filesystem;

/**
 * Created by Mike Grill on 4/20/15.
 *
 * Object representing devices
 */
public abstract class Device extends BaseObject {

    //type id for storage engines
    protected String type = "device";

    public Device(String absPath, long lastModified) {
        super(absPath, lastModified);
    }

    @Override
    public String getType() {
        return this.type;
    }

    public abstract String getDeviceType();

}
