package com.gogrillion.libgrafs.filesystem;

/**
 * Created by Mike Grill on 4/20/15.
 *
 * Object representing named pipes
 */
public class FIFO extends BaseObject {

    private String type = "fifo";

    public FIFO(String absPath, long lastModified) {
        super(absPath, lastModified);
    }

    @Override
    public String getType() {
        return this.type;
    }
}
