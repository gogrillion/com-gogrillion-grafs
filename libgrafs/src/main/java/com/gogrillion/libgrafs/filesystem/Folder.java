package com.gogrillion.libgrafs.filesystem;

/**
 * Created by Mike Grill on 4/20/15.
 *
 * Object representing a filesystem folder
 */
public class Folder extends BaseObject {

    private String type = "folder";
    private boolean isRoot;

    /**
     * Construct with default value for isRoot to false
     * @param absPath Absolute path of folder
     * @param lastModified Last modified timestamp as UNIX timestamp
     */
    public Folder( String absPath, long lastModified ){
        this(absPath, lastModified, false);
    }

    public Folder( String absPath, long lastModified, boolean isRoot ){
        //BaseObject values
        super(absPath, lastModified);

        //Folder-specific values
        this.isRoot = isRoot;
    }

    @Override
    public String getType() {
        return this.type;
    }

    public boolean isRoot(){
        return isRoot;
    }

}
