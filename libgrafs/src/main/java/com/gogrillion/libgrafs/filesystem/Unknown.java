package com.gogrillion.libgrafs.filesystem;

/**
 * Created by mgrill on 4/20/15.
 */
public class Unknown extends BaseObject {

    private String type = "unknown";

    public Unknown(String absPath, long lastModified) {
        super(absPath, lastModified);
    }

    @Override
    public String getType(){
        return this.type;
    }
}
