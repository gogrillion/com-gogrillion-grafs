# libgrafs

The purpose of this library is to store filesystem structure and objects in an
indexed graph database for analysis. Libgrafs uses the Titan graph database but could be
extended to use Neo4j or other popular graph databases.

# Usage

Creating / Opening a graph
GraFsDb will attempt to open a graph using files at {dbAbsoluteFolderPath}
If no database is found, a new graph will be created.

    import com.gogrillion.grafs.libgrafs.GraFsDb

    void main(String[] args]){

        //Create instance
        GraFs g = new GraFs();

        try{
            g.open( dbAbsoluteFolderPath );

            //..do stuff
            // g.addFile()
            // g.addPath()

        } catch( GraFsException ge ){

        }

        // always close
        g.close();
    }

# Recommendations

While adding files to the index, be sure to properly add *nix device files
as well as symlinks to prevent circular references.

